(asdf:defsystem #:demo-derive-agnostic-tests
  :name "demo-derive"
  :description "Manage data produced from the demos."
  :version "0.0.0"
  :author "Marek Kochanowicz"
  :defsystem-depends-on (:prove-asdf)
  :depends-on ( :prove
                :demo-derive-agnostic)
  :serial T
  :pathname "source"
  :components ((:module "produce"
                :components ((:test-file "producer-tests")))))
