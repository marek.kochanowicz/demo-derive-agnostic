(cl:defpackage #:demo-derive.common
  (:use #:cl #:demo-derive.aux-package)
  (:export
   #:memo-apply
   #:*memoized*))
