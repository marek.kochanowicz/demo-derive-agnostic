(cl:in-package #:demo-derive.common)


(defvar *memoized* nil)


(defun memo-apply (symbol &rest arguments)
  (if (null *memoized*)
      (apply symbol arguments)
      (ensure (gethash (cons symbol arguments) *memoized*)
        (apply symbol arguments))))
