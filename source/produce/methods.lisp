(cl:in-package #:demo-derive.produce)


(defmethod print-object ((object dependency-tree-node) stream)
  (print-unreadable-object (object stream :type t)
    (format stream "~a" (~> object producer name))))


(defmethod invoke-with-products-group/product/dependencies :around
    ((producer lisp-producer)
     products-group
     product
     dependencies
     extra-arguments)
  (let ((thread nil)
        (error nil))
    (cl-ds.utils:with-rebind (*memoization*
                              *log-stream*
                              *status-stream*
                              *config*
                              *producers-collection*
                              demo-derive.common:*memoized*
                              *top-level-memo-table*)
      (unwind-protect
           (progn
             (setf thread (bt:make-thread (lambda ()
                                            (cl-ds.utils:rebind
                                             (handler-case
                                                 (with-postgres-connection (*config*)
                                                   (maybe-with-transaction ()
                                                     (call-next-method)))
                                               (error (e)
                                                 (format *log-stream* "~a~%" e)
                                                 (setf error e)))))
                                          :name "Production thread."))
             (bt:join-thread thread)
             (setf thread nil)
             (when error
               (error error)))
        (when thread
          (ignore-errors (bt:interrupt-thread thread (lambda () (error 'terminated)))))))))


(defmethod invoke-with-products-group/product/dependencies :around
    ((producer fundamental-producer)
     products-group
     product
     dependencies
     extra-arguments)
  (if (eq (success product) t)
      (progn
        (format *status-stream*
                "Product with ID ~a already present, using it.~%"
                (id product))
        (force-output *status-stream*)
        product)
      (let ((success nil))
        (format *status-stream*
                "Generating ~a product with ID ~a.~%"
                (name producer)
                (id product))
        (force-output *status-stream*)
        (call-preamble extra-arguments
                       (demo-id product))
        (unwind-protect
             (progn
               (call-next-method)
               (setf success t))
          (if success
              (format *status-stream* "Success!~%")
              (format *status-stream* "Failure!~%"))
          (force-output *status-stream*)
          (call-cleanup extra-arguments
                        (demo-id product))))))


(defmethod make-postprocessing-product* ((controller (eql :postmodern))
                                         &rest initargs)
  (lret ((result (apply #'make 'postprocessing-product initargs)))
    (postmodern:save-dao result)))


(defmethod make-postprocessing-product-dependency* ((controller (eql :postmodern))
                                                    &rest initargs)
  (lret ((result (apply #'make 'postprocessing-product-dependency initargs)))
    (postmodern:save-dao result)))


(defmethod invoke-with-products-group/product/dependencies
    ((producer python-producer)
     products-group
     new-product
     dependencies
     extra-arguments)
  (if (null (create-path producer))
      nil
      (let* ((create-path (format nil "~a" (create-path producer)))
             (stream-output (if (boundp '*log-stream*)
                                *log-stream*
                                (make-broadcast-stream)))
             (process (~> (encode-python-json-argument
                           (id new-product)
                           (demo-id new-product)
                           (postgres-connection-list *config*)
                           dependencies)
                          (list "python3" create-path _)
                          (uiop:launch-program
                           :output stream-output
                           :error-output stream-output))))
        (setf *python* process)
        (let ((result (uiop:wait-process process)))
          (setf *python* nil)
          (unless (zerop result)
            (error "Python process failed."))))))


(defmethod existing-products* ((controller (eql :postmodern))
                               (producer single-input-producer)
                               (id integer)
                               producer-parameters)
  (let ((producer-parameters (if (null producer-parameters)
                                 :null
                                 (producer-parameters->json producer-parameters))))
    (postmodern:select-dao 'postprocessing-product
        (:and (:= 'producer_id (~> producer postgres-dao id))
              (:= 'demo_id (vector id))
              (:or (:and (:is-null producer-parameters)
                         (:is-null 'producer_parameters))
                   (:and (:not-null producer-parameters)
                         (:not-null 'producer_parameters)
                         (:<@ (:type producer-parameters jsonb)
                              'producer_parameters)
                         (:<@ 'producer_parameters
                              (:type producer-parameters jsonb))))))))


(defmethod existing-products* ((controller (eql :postmodern))
                               (producer single-input-producer)
                               (ids sequence)
                               producer-parameters)
  (let ((producer-parameters (if (null producer-parameters)
                                 :null
                                 (producer-parameters->json producer-parameters))))
    (~>> ids
         (map 'list
              (lambda (id)
                (postmodern:select-dao 'postprocessing-product
                    (:and (:= 'producer_id (~> producer postgres-dao id))
                          (:= 'demo_id (vector id))
                          (:or (:and (:is-null producer-parameters)
                                     (:is-null 'producer_parameters))
                               (:and (:not-null producer-parameters)
                                     (:not-null 'producer_parameters)
                                     (:<@ (:type producer-parameters jsonb)
                                          'producer_parameters)
                                     (:<@ 'producer_parameters
                                          (:type producer-parameters jsonb))))))))
         flatten)))


(defmethod existing-products* ((controller (eql :postmodern))
                               (producer multiple-input-producer)
                               (ids sequence)
                               producer-parameters)
  (let ((producer-parameters (if (null producer-parameters)
                                 :null
                                 (producer-parameters->json producer-parameters))))
    (postmodern:select-dao 'postprocessing-product
        (:and (:= 'function_name (name producer))
              (:= 'demo_id (coerce ids 'vector))
              (:or (:and (:is-null producer-parameters)
                         (:is-null 'producer_parameters))
                   (:and (:not-null producer-parameters)
                         (:not-null 'producer_parameters)
                         (:<@ (:type producer-parameters jsonb)
                              'producer_parameters)
                         (:<@ 'producer_parameters
                              (:type producer-parameters jsonb))))))))


(defmethod register-producer* ((collection producers-collection)
                               (producer fundamental-producer))
  (let ((map (name-to-producer-dictionary collection))
        (name (name producer)))
    (restart-case
        (bind (((:values existing found) (gethash name map)))
          (when (eq existing producer)
            (return-from register-producer* collection))
          (when found
            (error 'producer-name-not-unique
                   :name name))
          (setf (gethash name map) producer)
          producer)
      (dont-register () (gethash name map))
      (replace-producer ()
        (remhash name map)
        (register-producer* collection producer)))))


(defmethod find-producer* ((collection producers-collection)
                           (name string))
  (or (gethash name (name-to-producer-dictionary collection))
      (error 'producer-name-not-found :name name)))


(defmethod find-producer-by-id* ((collection producers-collection)
                                 id)
  (or (block v
        (maphash-values (lambda (producer)
                          (when (= (id (postgres-dao producer)) id)
                            (return-from v producer)))
                        (name-to-producer-dictionary collection))
        nil)
      (error 'producer-id-not-found :id id)))


(defmethod find-producer* ((collection producers-collection)
                           (producer fundamental-producer))
  producer)


(defmethod direct-dependencies :around ((producer fundamental-producer))
  (let ((dependencies (call-next-method)))
    (values (mapcar (lambda (x)
                      (if (consp x)
                          (find-producer (car x))
                          (find-producer x)))
                    dependencies)
            (mapcar (lambda (x)
                      (if (consp x)
                          (list (cdr x))
                          nil))
                    dependencies))))


(defmethod dependencies-tree* ((producer single-input-producer)
                               (ids sequence)
                               arguments
                               dependency-producers
                               &optional parent)
  (mapcar (lambda (id) (dependencies-tree* producer id arguments dependency-producers parent))
          ids))


(defmethod dependencies-tree* ((producer single-input-producer)
                               (ids integer)
                               arguments
                               dependency-producers
                               &optional parent)
  (lret ((result
          (make 'dependency-tree-node
                :producer producer
                :parent parent
                :memo-table (or (if (null parent)
                                    (and *memoization* *top-level-memo-table*)
                                    (memo-table parent))
                                (and *memoization* (make-hash-table :test 'equal)))
                :ids ids
                :arguments (cl-ds:at arguments (name producer)))))
    (setf (direct-dependencies result)
          (make-direct-dependencies-tree-nodes producer
                                               ids
                                               arguments
                                               result
                                               dependency-producers))))


(defmethod dependencies-tree* ((producer multiple-input-producer)
                               (ids sequence)
                               arguments
                               dependency-producers
                               &optional parent)
  (lret ((result
          (make 'dependency-tree-node
                :producer producer
                :parent parent
                :memo-table (or (if (null parent)
                                    (and *memoization* *top-level-memo-table*)
                                    (memo-table parent))
                                (and *memoization* (make-hash-table :test 'equal)))
                :ids ids
                :arguments (cl-ds:at arguments (name producer)))))
    (setf (direct-dependencies result)
          (make-direct-dependencies-tree-nodes producer
                                               ids
                                               arguments
                                               result
                                               dependency-producers))))


(defmethod dependencies-tree* ((producer multiple-input-producer)
                               (ids integer)
                               arguments
                               dependency-producers
                               &optional parent)
  (dependencies-tree* producer (list ids) arguments dependency-producers parent))


(defmethod send-producers-collection-to-database* ((collection producers-collection))
  (maphash (lambda (name producer)
             (setf (postgres-dao producer)
                   (or (first (postmodern:select-dao 'postprocessing-producer
                                  (:= 'name name)))
                       (lret ((result (make-instance 'postprocessing-producer
                                                     :name name)))
                         (postmodern:save-dao result))))
             (construct-producer-schema producer))
           (name-to-producer-dictionary collection)))


(defmethod fetch-producers-collection-from-database* ((collection producers-collection))
  (maphash (lambda (name producer)
             (setf (postgres-dao producer)
                   (or (first (postmodern:select-dao 'postprocessing-producer
                                                     (:= 'name name)))
                       (error 'producer-name-not-found "No producer with name ~a!" name))))
           (name-to-producer-dictionary collection)))


(defmethod construct-producer-schema ((producer fundamental-producer))
  (when-let ((path-to-schema (path-to-schema producer)))
    (postmodern:execute-file path-to-schema)))


(defmethod construct-producer-schema ((producer lisp-producer))
  (dolist (query (sql-code producer))
    (postmodern:execute query)))


(defmethod print-object ((object postprocessing-products-group) stream)
  (print-unreadable-object (object stream)
    (format stream "Products group id: ~a, success: ~a"
            (id object)
            (success object))))


(defmethod make-postprocessing-products-group* ((controller (eql :postmodern)))
  (lret ((result (make 'postprocessing-products-group)))
    (postmodern:save-dao result)))


(defmethod update-dao* :before ((controller (eql :postmodern))
                                (product postprocessing-product))
  (bind (((:accessors production-date) product))
    ;; hack for bug in the postmodern-localtime
    (when (integerp production-date)
      (setf production-date (local-time:unix-to-timestamp production-date)))))


(defmethod update-dao* ((controller (eql :postmodern)) object)
  (postmodern:update-dao object))


(defmethod dependency-tree-node-product-key ((node dependency-tree-node))
  (product-key (producer node)
               (ids node)
               (arguments node)))


(defmethod dependency-tree-node-completed-product ((node dependency-tree-node))
  (cons (producer node)
        (lret ((result (completed-product (producer node) (ids node) (arguments node))))
          (assert result))))


(defmethod delete-product* ((controller (eql :postmodern))
                            producer
                            product)
  (postmodern:execute (:delete-from 'postprocessing.product_dependencies
                       :where (:= 'dependent (demo-derive.produce:id product))))
  (postmodern:execute (:delete-from 'postprocessing.products_group_product
                       :where (:= 'product_id (demo-derive.produce:id product))))
  (flet ((impl (table-name)
           (postmodern:execute (:delete-from (~> table-name string-upcase make-symbol)
                                :where (:= 'product_id (demo-derive.produce:id product))))))
    (let ((table-name (table-name producer)))
      (typecase table-name
        (string (impl table-name))
        (vector (map nil #'impl table-name))))
    (postmodern:delete-dao product)))


(defmethod make-products-group-state* ((controller (eql :postmodern))
                                       products-group
                                       dependencies-tree
                                       &key (automatic-deletion nil))
  (let ((result nil))
    (tagbody start
       (handler-case (postmodern:with-transaction (nil :repeatable-read-rw)
                       (setf result (call-next-method)))
         (postmodern:database-error (e)
           (if (member (postmodern:database-error-code e)
                       '("40001" "40P01")
                       :test #'equal)
               (go start)
               (error e)))))
    result))


(defmethod completedp* ((state products-group-state))
  (zerop (length (pending-products state))))


(defmethod select-pending* ((state products-group-state))
  (assert (not (completedp* state)))
  (bind (((:accessors pending-products-pointer pending-products) state))
    (setf pending-products-pointer (mod (1+ pending-products-pointer)
                                        (fill-pointer pending-products)))))


(defmethod sync-dao* ((controller (eql :postmodern))
                      object)
  (postmodern:get-dao (type-of object) (id object)))


(defmethod lock-product* ((controller (eql :postmodern))
                          product)
  (postmodern:execute
   (format nil "select from postprocessing.products where id = ~a for update;"
           (id product))))


(defmethod signal-product-error :around (producer node &key reason)
  (if (typep reason 'unrecoverable-error)
      (error reason)
      (call-next-method)))


(defmethod signal-product-error ((producer single-input-producer)
                                 node
                                 &key reason)
  (let ((ids (ids node)))
    (assert ids)
    (assert (atom ids))
    (error 'single-demo-failed
           :reason reason
           :node node
           :id ids)))


(defmethod signal-product-error ((producer multiple-input-producer)
                                 node
                                 &key reason)
  (assert reason)
  (error reason))


(defmethod directly-dependent-nodes ((state products-group-state) node)
  (gethash (dependency-tree-node-product-key node) (dependent-map state)))


(defmethod dependent-nodes ((state products-group-state) node)
  (bind ((dependent-map (dependent-map state))
         ((:flet dependent (node))
          (gethash (dependency-tree-node-product-key node) dependent-map)))
    (~> (gethash (dependency-tree-node-product-key node) (dependent-map state))
        (cl-ds.alg:multiplex
         :function (lambda (node)
                     (cl-ds:xpr (:stack (list node))
                       (when (endp stack)
                         (cl-ds:finish))
                       (bind (((first . rest) stack))
                         (cl-ds:send-recur first
                                           :stack (append (dependent first)
                                                          rest))))))
        cl-ds.alg:to-list)))


(defmethod fullfill-node ((node dependency-tree-node) state)
  (bind (((:accessors producer direct-dependencies ids product
                      using-existing-product parent memo-table
                      failure arguments)
          node)
         ((:accessors completed-products arguments) state)
         (demo-derive.common:*memoized* memo-table))
    (when using-existing-product
      (maybe-with-transaction ()
        (lock-product product)
        (setf product (sync-dao product))
        (switch ((success product))
          (:null (sleep 5) (error 'product-not-ready-yet))
          (t (return-from fullfill-node nil))
          (nil (if (eql (canceled product) t)
                   (progn               ; retry canceled products
                     (setf (canceled product) :null
                           (logs product) ""
                           (success product) :null)
                     (update-dao product))
                   (signal-product-error producer node :reason "Failed elsewhere."))))))
    (handler-case
        (let ((dependencies (~> direct-dependencies
                                (cl-ds.alg:on-each (lambda (node)
                                                     (cons (producer node)
                                                           (gethash (dependency-tree-node-product-key node)
                                                                    completed-products))))
                                (cl-ds.alg:group-by :key #'car :test 'eq)
                                (cl-ds.alg:on-each #'cdr)
                                (cl-ds.alg:without #'null)
                                cl-ds.alg:to-vector
                                cl-ds.alg:to-list)))
          (iterate
            (declare (ignorable producer))
            (for (producer . deps) in dependencies)
            (iterate
              (for d in-vector deps)
              (make-postprocessing-product-dependency
               :dependent (id product)
               :depends-on (id d))))
          (let ((*current-product* product))
            (invoke-with-products-group/product/dependencies
             producer
             (products-group state)
             product
             dependencies
             (arguments node))
            (setf (success product) t)))
      (terminated (e)
        (setf (canceled product) t)
        (error e))
      (error (e)
        (setf (canceled product) nil)
        (format *log-stream* "~a~%" e)
        (signal-product-error producer node :reason e)))))


(defmethod try-fullfill* ((state products-group-state) index
                          &aux (pending-products (pending-products state)))
  (check-type index non-negative-integer)
  (assert (< index (length pending-products)))
  (bind ((pending-node (aref pending-products index))
         ((:accessors product) pending-node)
         (success nil)
         (now nil)
         (finish-time nil)
         (retry nil)
         (*log-stream* (make-string-output-stream))
         (completed-products (completed-products state)))
    (unwind-protect
         (tagbody start
            (restart-case
                (progn
                  (setf retry nil)
                  (setf now (get-universal-time))
                  (fullfill-node pending-node state)
                  (setf finish-time (get-universal-time))
                  (cl-ds.utils:swapop pending-products index)
                  (setf (gethash (dependency-tree-node-product-key pending-node)
                                 completed-products)
                        product)
                  (setf success t)
                  (propagate-success pending-node state)
                  (go end))
              (retry-now ()
                (go start))
              (retry-later ()
                (setf retry t
                      success t)
                (go end))
              (fail-product ()
                (setf (failure pending-node) t
                      success nil
                      (success product) nil)
                (cl-ds.utils:swapop pending-products index)
                (propagate-failure pending-node state)
                (go end))
              (skip-demo-id ()
                (setf (failure pending-node) t
                      (success product) nil
                      success t)
                (cl-ds.utils:swapop pending-products index)
                (propagate-success pending-node state)
                (go end)))
          end)
      (unless retry
        (let ((logs (get-output-stream-string *log-stream*))
              (canceled (canceled product)))
          (with-transaction-retry
              (maybe-with-transaction (nil :repeatable-read-rw)
                (lock-product product)
                (setf product (sync-dao product))
                (setf (logs product) logs)
                (when (and now finish-time)
                  (setf (processing-time product) (- finish-time now)))
                (setf (canceled product) (if (eql :null canceled)
                                             nil
                                             canceled))
                (setf (success product) success)
                (update-dao product))))))
    success))


(defmethod direct-dependency-nodes ((state products-group-state)
                                    (node dependency-tree-node))
  (gethash (dependency-tree-node-product-key node)
           (dependencies-map state)))


(defmethod (setf direct-dependency-nodes) (new-value
                                           (state products-group-state)
                                           (node dependency-tree-node))
  (setf (gethash (dependency-tree-node-product-key node) (dependencies-map state))
        new-value))


(defmethod dependency-nodes ((state products-group-state)
                             (node dependency-tree-node))
  (~> (direct-dependency-nodes state node)
      (cl-ds.alg:multiplex
       :function (lambda (node)
                   (cl-ds:xpr (:stack (list node))
                     (when (endp stack)
                       (cl-ds:finish))
                     (bind (((first . rest) stack))
                       (cl-ds:send-recur first
                                         :stack (append (direct-dependency-nodes state
                                                                                 first)
                                                        rest))))))
      cl-ds.alg:to-list))


(defmethod remove-from-waiting ((state products-group-state)
                                (node dependency-tree-node))
  (let* ((waiting (waiting-products state))
         (waiting-position (position node waiting :test 'eq)))
    (unless (null waiting-position)
      (cl-ds.utils:swapop waiting waiting-position))))


(defmethod remove-from-pending ((state products-group-state)
                                (node dependency-tree-node))
  (let* ((pending (pending-products state))
         (pending-position (position node pending :test 'eq)))
    (unless (null pending-position)
      (assert pending-position)
      (cl-ds.utils:swapop pending pending-position))))


(defmethod move-from-waiting-to-pending ((state products-group-state)
                                         (node dependency-tree-node))
  (let* ((waiting (waiting-products state))
         (pending (pending-products state))
         (waiting-position (position node waiting :test 'eq)))
    (unless (null waiting-position)
      (cl-ds.utils:swapop waiting waiting-position)
      (vector-push-extend node pending))))


(defmethod propagate-success ((node dependency-tree-node) state)
  (cl-progress-bar:update 1)
  (iterate
    (for dependent in (directly-dependent-nodes state node))
    (for dependencies = (direct-dependency-nodes state dependent))
    (for new = (remove node dependencies
                       :test (lambda (a b)
                               (equal (dependency-tree-node-product-key a)
                                      (dependency-tree-node-product-key b)))))
    (setf (direct-dependency-nodes state dependent) new)
    (for now-pending-p = (emptyp new))
    (when now-pending-p
      (move-from-waiting-to-pending state dependent))))


(defmethod propagate-failure ((node dependency-tree-node) state)
  (cl-progress-bar:update 1)
  (iterate
    (for dependent in (dependent-nodes state node))
    (for product = (product dependent))
    (setf (failure dependent) t
          (canceled (product dependent)) (canceled (product node))
          (success product) nil)
    (cl-progress-bar:update 1)
    (update-dao product)
    (remove-from-waiting state dependent)))


(defmethod make-products-group-state* (controller
                                       products-group
                                       dependencies-forest
                                       &key (automatic-deletion nil)
                                       &aux (seen-nodes (make-hash-table :test 'equal)))
  (lret ((*products-group-state* (make 'products-group-state
                                       :automatic-deletion automatic-deletion
                                       :products-group products-group)))
    (iterate
      (for dependencies-tree in (flatten dependencies-forest))
      (dependency-tree-node-map
       (lambda (node &aux (product-key (dependency-tree-node-product-key node)))
         (let* ((deps (direct-dependencies node))
                (producer (producer node))
                (arguments (arguments node))
                (ids (ids node))
                (rootp (root-node-p node))
                (dependencies-map (dependencies-map *products-group-state*))
                (dependent-map (dependent-map *products-group-state*))
                (existing-dependencies (gethash product-key dependencies-map '()))
                (new-dependencies (union deps
                                         existing-dependencies
                                         :test 'equal
                                         :key #'dependency-tree-node-product-key))
                (parent (parent node))
                (existing-dependent (gethash product-key
                                             dependent-map))
                (new-dependent (union (if parent
                                          (list parent)
                                          (list))
                                      existing-dependent
                                      :key #'dependency-tree-node-product-key
                                      :test 'equal))
                (number-of-deps (length new-dependencies))
                (using-existing-product nil)
                (existing-products (existing-products* controller producer
                                                       ids arguments))
                (registered (registered-product producer ids arguments))
                (existing-product (find-if
                                   (lambda (product)
                                     (or (member (success product) '(t :null))
                                         (and (not (success product))
                                              (canceled product))))
                                   existing-products)))
           (unless registered
             (restart-case
                 (when existing-product
                   (error 'already-existing
                          :producer producer
                          :product existing-product
                          :postgres-dao existing-product))
               (use-existing-product ()
                 (format *group-log-stream* "Using existing product ~a.~%" (name producer))
                 (setf (completed-product producer ids arguments) existing-product
                       (product node) existing-product
                       using-existing-product t))
               (replace-existing-product ()
                 (format *group-log-stream* "Replacing product ~a.~%" (name producer))
                 (delete-product* controller producer (first existing-products)))
               (create-new-product ()
                 (format *group-log-stream* "Creating new product ~a, despite existing one being present.~%" (name producer)))))
           (setf (gethash product-key dependencies-map) new-dependencies
                 (gethash product-key dependent-map) new-dependent
                 (using-existing-product node) using-existing-product)
           (unless using-existing-product
             (setf (product node)
                   (or registered
                       (make-postprocessing-product
                        :demo-id (let ((ids (ids node)))
                                   (if (typep ids 'sequence)
                                       (coerce ids 'vector)
                                       (vector ids)))
                        :producer-id (~> producer postgres-dao id)
                        :producer-parameters (if (null arguments)
                                                 :null
                                                 (producer-parameters->json arguments))
                        :products-group-id (id products-group)))))
           (when (or rootp using-existing-product)
             (setf (gethash (product node) (preserved-products *products-group-state*)) t))
           (unless (shiftf (gethash product-key seen-nodes) t)
             (postmodern:execute (:insert-into '#:postprocessing.products_group_product
                                  :set
                                  '#:products_group_id (id products-group)
                                  '#:product_id (~> node product id)))
             (if (or (zerop number-of-deps) using-existing-product)
                 (vector-push-extend node (pending-products *products-group-state*))
                 (vector-push-extend node (waiting-products *products-group-state*))))
           (register-product producer ids arguments (product node))
           (not using-existing-product)))
       dependencies-tree))))


(defmethod fullfill-products-group-state* (controller products-group-state)
  (cl-progress-bar:with-progress-bar ((+ (~> products-group-state
                                             waiting-products
                                             length)
                                         (~> products-group-state
                                             pending-products
                                             length))
                                      "Generating products group ID ~a"
                                      (~> products-group-state
                                          products-group
                                          id))
    (let ((total-success t)
          (products-group (products-group products-group-state)))
      (unwind-protect
           (handler-bind
               ((warning
                  (lambda (x) (declare (ignore x))
                    (invoke-restart 'muffle-warning)))
                (terminated
                  (lambda (x) (declare (ignore x))
                    (setf (canceled products-group-state) t))))
             (progn
               (iterate
                 (until (completedp* products-group-state))
                 (for next = (select-pending* products-group-state))
                 (for success-now = (try-fullfill* products-group-state next))
                 (setf total-success (and total-success success-now))
                 (trivial-garbage:gc :full t))
               (when (automatic-deletion products-group-state)
                 (maphash-values (lambda (product)
                                   (unless (gethash product (preserved-products products-group-state))
                                     (delete-product (~> product producer-id find-producer-by-id)
                                                     product)))
                                 (completed-products products-group-state)))))
        ;; update on this dao is performed in the calling function
        (setf (success products-group) total-success)
        ;; just ensure that products that had no chance to run have a proper status
        (flet ((fail-product (node &aux (product (product node)))
                 (unless (using-existing-product node)
                   (with-transaction-retry
                     (setf product (sync-dao product))
                     (when (and (eql (success product) :null))
                       (setf (success product) nil
                             (canceled product) (canceled products-group-state)))
                     (update-dao product)))))
          (iterate
            (for waiting in-vector (waiting-products products-group-state))
            (fail-product waiting))
          (iterate
            (for pending in-vector (pending-products products-group-state))
            (fail-product pending)))
        (when *python*
          (ignore-errors (uiop:run-program (format nil "rkill -15 ~a" (uiop:process-info-pid *python*)))))))))


(defmethod initialize-instance :after ((producer lisp-producer)
                                       &rest rest)
  (declare (ignore rest))
  (ensure (sql-code producer)
    (postmodern:read-queries (path-to-schema producer))))


(defmethod print-object ((product postprocessing-product)
                         stream)
  (print-unreadable-object (product stream :type t)
    (format stream "~a" (id product))))


(defmethod root-node-p ((node dependency-tree-node))
  (null (parent node)))
