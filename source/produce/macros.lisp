(cl:in-package #:demo-derive.produce)


(defmacro with-postgres-connection ((config) &body body)
  (once-only (config)
    (with-gensyms (!thunk)
      `(flet ((,!thunk ()
                ,@body))
         (if (and (eql :postmodern (dao-controller *config*))
                  (slot-boundp ,config '%postgres-connection-list)
                  (null postmodern:*database*))
             (postmodern:with-connection (postgres-connection-list ,config)
               (,!thunk))
             (,!thunk))))))


(defmacro maybe-with-transaction (arguments &body body)
  (with-gensyms (!thunk)
    `(flet ((,!thunk ()
              ,@body))
       (if (eql :postmodern (dao-controller *config*))
           (postmodern:with-transaction ,arguments
             (,!thunk))
           (,!thunk)))))


(defmacro define-lisp-producer (variable-name form)
  (with-gensyms (!object)
    `(defvar ,variable-name
       (lret ((,!object ,form))
         (register-producer ,!object)))))


(defmacro with-transaction-retry (&body body)
  (with-gensyms (!thunk !start)
    `(flet ((,!thunk ()
              ,@body))
       (tagbody ,!start
          (handler-case (,!thunk)
            (postmodern:database-error (e)
              (if (member (postmodern:database-error-code e)
                          '("40001" "40P01")
                          :test #'equal)
                  (go ,!start)
                  (error e))))))))
