(cl:in-package #:demo-derive.produce)


(defun ids-list (ids)
  (if (typep ids 'sequence)
      (coerce ids 'list)
    (list ids)))


(defun product-key (producer ids arguments)
  (list producer (ids-list ids) arguments))


(defun completed-product (producer ids producer-parameters)
  (gethash (product-key producer ids producer-parameters)
           (completed-products *products-group-state*)))


(defun registered-product (producer ids producer-parameters)
  (gethash (product-key producer ids producer-parameters)
           (registered-products *products-group-state*)))


(defun register-product (producer ids producer-parameters product)
  (setf (gethash (product-key producer ids producer-parameters)
                 (registered-products *products-group-state*))
        product))


(defun (setf completed-product) (new-product producer ids producer-parameters)
  (setf (gethash (product-key producer ids producer-parameters)
                 (completed-products *products-group-state*))
        new-product))


(defun escape-string (string)
  (serapeum:escape string (lambda (char)
                            (if (serapeum:whitespacep char)
                                "\\ "
                                (format nil "\\~a" char)))))


(defun encode-python-json-argument (product-id demo-id
                                    postgres-connection-list
                                    dependencies)
  (bind (((database-name user password host . rest) postgres-connection-list))
    (declare (ignore rest))
    (with-output-to-string (stream)
      (yason:encode (dict "product_id" product-id
                          "demo_id" demo-id
                          "postgres_connection" (dict "database_name" database-name
                                                      "user" user
                                                      "password" password
                                                      "host" host)
                          "dependencies" (~> dependencies
                                             (cl-ds.alg:on-each
                                              (lambda (producer.products)
                                                (cons (~> producer.products car name)
                                                      (~>> producer.products
                                                           cdr
                                                           (map 'vector #'id)))))
                                             (cl-ds.alg:to-hash-table
                                              :hash-table-key #'car
                                              :hash-table-value #'cdr
                                              :test #'equal)))
                    stream))))

(defun sort-producer-parameters (parameters)
  (labels ((sort-inner (argument)
             (if (listp argument)
                 (iterate
                   (for (key . value) in (sort argument #'string< :key #'first))
                   (collecting (cons key (sort-inner value))))
                 argument)))
    (sort-inner parameters)))


(defun producer-parameters->json (parameters)
  (labels ((inner (argument)
             (if (listp argument)
                 (iterate
                   (with result = (make-hash-table :test 'equal))
                   (for (key . value) in argument)
                   (when (stringp key)
                     (setf (gethash key result) (inner value)))
                   (finally (return result)))
                 argument)))
    (with-output-to-string (stream)
      (yason:encode (inner parameters)
                    stream))))


(defun compile-preamble/cleanup (value)
  (with-input-from-string (stream value)
    (compile nil (read stream))))


(defun parsed-json->producer-parameters (object)
  (labels ((inner (argument)
             (if (hash-table-p argument)
                 (~>> argument
                      hash-table-alist
                      (mapcar (lambda (key.value)
                                (bind (((key . value) key.value))
                                  (cond ((string= key ":PREAMBLE")
                                         (cons :preamble (compile-preamble/cleanup value)))
                                        ((string= key ":CLEANUP")
                                         (cons :cleanup (compile-preamble/cleanup value)))
                                        (t (cons key (inner value))))))))
                 argument)))
    (~> object
        inner
        sort-producer-parameters)))


(defun json->producer-parameters (json)
  (~> json
      yason:parse
      parsed-json->producer-parameters))


(defun augment-extra-arguments (arguments name extra-arguments)
  (lret ((result (cl-ds:replica extra-arguments)))
    (setf (cl-ds:at result name) (sort-producer-parameters arguments))))
