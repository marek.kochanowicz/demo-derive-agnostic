(cl:in-package #:demo-derive.produce)


(defclass postprocessing-producer ()
  ((id :initarg :id
       :col-type serial
       :reader id)
   (name :initarg :name
         :col-type varchar
         :reader name))
  (:metaclass postmodern:dao-class)
  (:table-name "postprocessing.producers")
  (:keys id))


(defclass postprocessing-products-group ()
  ((id :initarg :id
       :col-type serial
       :reader id)
   (success :initarg :success
            :col-type bool
            :accessor success)
   (information :initarg :information
                :col-type jsonb
                :accessor information)
   (logs :initarg :logs
         :col-type varchar
         :accessor logs)
   (expiration_date :initarg :expiration-date
                    :col-type timestampz
                    :accessor expiration-date))
  (:metaclass postmodern:dao-class)
  (:table-name "postprocessing.products_groups")
  (:default-initargs :success :null
                     :information :null
                     :expiration-date :null)
  (:keys id))


(defclass postprocessing-product ()
  ((id :initarg :id
       :col-type serial
       :reader id)
   (producer_id :initarg :producer-id
                :col-type int4
                :reader producer-id)
   (products_group_id :initarg :products-group-id
                      :col-type int4
                      :reader products-group-id)
   (demo_id :initarg :demo-id
            :col-type int4[]
            :reader demo-id)
   (success :initarg :success
            :initform :null
            :col-type bool
            :accessor success)
   (production_date :intiarg :production-date
                    :col-type timestampz
                    :initform (local-time:now)
                    :accessor production-date)
   (producer_parameters :initarg :producer-parameters
                        :reader producer-parameters
                        :col-type jsonb)
   (logs :initarg :logs
         :initform :null
         :col-type varchar
         :accessor logs)
   (processing_time :initarg :processing-time
                    :col-type int4
                    :initform :null
                    :accessor processing-time)
   (canceled :initarg :canceled
             :col-type bool
             :initform :null
             :accessor canceled))
  (:default-initargs :success :null
                     :production-date (local-time:now))
  (:metaclass postmodern:dao-class)
  (:table-name "postprocessing.products")
  (:keys id))


(defclass postprocessing-product-dependency ()
  ((id :initarg :id
       :col-type serial
       :reader id)
   (dependent :initarg :dependent
              :col-type int4
              :reader dependent)
   (depends_on :initarg :depends-on
               :col-type int4
               :reader depends-on))
  (:metaclass postmodern:dao-class)
  (:table-name "postprocessing.product_dependencies")
  (:keys id))


(closer-mop:ensure-finalized (find-class 'postprocessing-producer))
(closer-mop:ensure-finalized (find-class 'postprocessing-product))
(closer-mop:ensure-finalized (find-class 'postprocessing-product-dependency))
(closer-mop:ensure-finalized (find-class 'postprocessing-products-group))


(defclass producers-collection ()
  ((%name-to-producer-dictionary :initarg :name-to-producer-dictionary
                                 :reader name-to-producer-dictionary))
  (:default-initargs :name-to-producer-dictionary (make-hash-table :test 'equal)))


(defclass fundamental-config ()
  ((%postgres-connection-list :initarg :postgres-connection-list
                              :reader postgres-connection-list)
   (%python-producers-path :initarg :python-producers-path
                           :reader python-producers-path)
   (%dao-controller :initarg :dao-controller
                    :reader dao-controller))
  (:default-initargs
   :dao-controller :postmodern
   :python-producers-path (~>> (asdf:system-source-directory :demo-derive)
                               (merge-pathnames "python-producers"))))


(defclass single-table-producer ()
  ())


(defclass fundamental-producer ()
  ((%name :initarg :name
          :reader name)
   (%path-to-schema :initarg :path-to-schema
                    :reader path-to-schema)
   (%direct-dependencies :initarg :direct-dependencies
                         :reader direct-dependencies)
   (%table-name :initarg :table-name
                :reader table-name)
   (%postgres-dao :initarg :postgres-dao
                  :accessor postgres-dao))
  (:default-initargs :direct-dependencies '()
                     :path-to-schema nil
                     :table-name nil
                     :postgres-dao nil))


(defclass single-input-producer (fundamental-producer)
  ())


(defclass multiple-input-producer (fundamental-producer)
  ())


(defclass lisp-producer (fundamental-producer)
  ((%sql-code :initarg :sql-code
              :initform nil
              :accessor sql-code)))


(defclass single-input-lisp-producer (lisp-producer single-input-producer)
  ())


(defclass multiple-input-lisp-producer (lisp-producer multiple-input-producer)
  ())


(defclass python-producer (fundamental-producer)
  ((%create-path :initarg :create-path
                 :reader create-path)
   (%delete-path :initarg :delete-path
                 :reader delete-path)))


(defclass single-input-python-producer (python-producer single-input-producer)
  ())


(defclass single-table-single-input-python-producer (single-table-producer
                                                     single-input-python-producer)
  ())


(defclass multiple-input-python-producer (python-producer multiple-input-producer)
  ())


(defclass single-table-multiple-input-python-producer (single-table-producer
                                                       multiple-input-lisp-producer)
  ())


(define-condition unrecoverable-error (error)
  ())


(define-condition unrecoverable-products-group-error (unrecoverable-error)
  ())


(define-condition producer-name-not-unique (error)
  ((%name :initarg :name
          :reader name))
  (:report (lambda (condition stream)
             (format stream "Name ~a is not unique." (name condition)))))


(define-condition producer-name-not-found (unrecoverable-error)
  ((%name :initarg :name
          :reader name))
  (:report (lambda (condition stream)
             (format stream
                     "No producer with name ~a could be found."
                     (name condition)))))


(define-condition producer-id-not-found (unrecoverable-error)
  ((%id :initarg :id
        :reader id))
  (:report (lambda (condition stream)
             (format stream
                     "No producew with id ~a could be found."
                     (id condition)))))


(define-condition circular-dependency (unrecoverable-products-group-error)
  ())


(define-condition already-existing (error)
  ((%producer :initarg :producer
              :reader producer)
   (%product :initarg :product
             :reader product)
   (%postgres-dao :initarg :postgres-dao
                  :reader postgres-dao))
  (:report (lambda (object stream)
             (format stream "Product ~a already present with id ~a!"
                     (~> object producer name)
                     (~> object postgres-dao id)))))


(define-condition single-demo-failed (error)
  ((%id :initarg :id
        :reader id)
   (%reason :initarg :reason
            :reader reason)
   (%node :initarg :node
          :reader node))
  (:report (lambda (object stream)
             (format stream "~a" (reason object)))))


(define-condition products-group-failed (error)
  ())


(define-condition product-not-ready-yet (error)
  ())


(define-condition terminated (error)
  ())


(defclass products-group-state ()
  ((%completed-products :initarg :completed-products
                        :reader completed-products)
   (%registered-products :initarg :registered-products
                         :reader registered-products)
   (%pending-products-pointer :initarg :pending-products-pointer
                              :accessor pending-products-pointer)
   (%pending-products :initarg :pending-products
                      :reader pending-products)
   (%waiting-products :initarg :waiting-products
                      :reader waiting-products)
   (%products-group :initarg :products-group
                    :reader products-group)
   (%dependencies-map :initarg :dependencies-map
                      :reader dependencies-map)
   (%dependent-map :initarg :dependent-map
                   :reader dependent-map)
   (%arguments :initarg :arguments
               :reader arguments)
   (%canceled :initarg :canceled
              :initform nil
              :accessor canceled)
   (%automatic-deletion :initarg :automatic-deletion
                        :reader automatic-deletion)
   (%preserved-products :initarg :preserved-products
                        :reader preserved-products)
   (%expiration-date :initarg :expiration-date
                     :initform nil
                     :accessor expiration-date))
  (:default-initargs
   :preserved-products (make-hash-table :test 'eq)
   :automatic-deletion nil
   :completed-products (make-hash-table :test 'equal)
   :registered-products (make-hash-table :test 'equal)
   :dependent-map (make-hash-table :test 'equal)
   :arguments (extra-arguments)
   :waiting-products (vect)
   :pending-products (vect)
   :pending-products-pointer -1
   :dependencies-map (make-hash-table :test 'equal)))


(defclass dependency-tree-node ()
  ((%producer :initarg :producer
              :reader producer)
   (%direct-dependencies :initarg :direct-dependencies
                         :accessor direct-dependencies)
   (%ids :initarg :ids
         :reader ids)
   (%product :initarg :product
             :accessor product)
   (%using-existing-product :initarg :using-existing-product
                            :accessor using-existing-product)
   (%parent :initarg :parent
            :reader parent)
   (%memo-table :initarg :memo-table
                :reader memo-table)
   (%failure :initarg :failure
             :accessor failure)
   (%arguments :initarg :arguments
               :reader arguments))
  (:default-initargs
   :using-existing-product nil
   :direct-dependencies nil
   :memo-table nil
   :failure nil
   :product nil))
