(cl:in-package #:demo-derive.produce)


(defclass mock-id-holder ()
  ((%id :initarg :id
        :reader id)
   (%logs :initarg :logs
          :initform :null
          :accessor logs)
   (%demo-id :initarg :demo-id
             :reader demo-id)
   (%success :accessor success
             :initarg :success))
  (:default-initargs
   :id 0
   :success nil))


(defclass mock-producer (fundamental-producer)
  ((%invoke-counter :accessor invoke-counter
                    :initarg :invoke-counter)
   (%existing-products :initarg :existing-products
                       :reader existing-products-impl))
  (:default-initargs :invoke-counter 0
                     :postgres-dao (make 'mock-id-holder)
                     :existing-products '()))


(defclass single-input-mock-producer (single-input-producer
                                      mock-producer)
  ())


(defclass mock-dependency-tree-node (dependency-tree-node)
  ((%fullfilled :initform nil
                :accessor fullfilled)
   (%future-error :initarg :future-error
                  :initform nil
                  :reader future-error)))


(defmethod fullfill-node ((node mock-dependency-tree-node) state)
  (bind (((:accessors fullfilled future-error) node))
    (when future-error
      (signal-product-error (producer node)
                            node
                            :reason future-error))
    (setf fullfilled t)))


(defmethod invoke-with-products-group/product/dependencies* ((producer mock-producer)
                                                             products-group
                                                             product
                                                             dependencies
                                                             extra-arguments)
  (incf (invoke-counter producer))
  (make 'mock-id-holder))


(defmethod make-postprocessing-products-group* ((controller (eql :mock)))
  (make 'mock-id-holder))


(defmethod update-dao* ((controller (eql :mock))
                        dao)
  dao)


(defmethod make-postprocessing-product* ((controller (eql :mock)) &rest initargs)
  (make 'mock-id-holder :demo-id (getf initargs :demo-id)))


(defmethod make-postprocessing-product-dependency* ((controller (eql :mock))
                                                    &rest initargs)
  initargs)


(defmethod existing-products* ((controller (eql :mock)) (producer mock-producer) ids producer-parameters)
  (existing-products-impl producer))


(prove:plan 47)

(let* ((producer (make 'fundamental-producer
                       :name "test-producer"))
       (dependent-producer (make 'fundamental-producer
                                 :name "dependent"
                                 :direct-dependencies (list "test-producer")))
       (*producers-collection* (make 'producers-collection)))
  (register-producer* *producers-collection* producer)
  (register-producer* *producers-collection* dependent-producer)
  (prove:is (first (direct-dependencies dependent-producer))
            producer))

(let* ((producer (make 'single-input-producer
                       :name "test-producer"
                       :direct-dependencies (list "dependent")))
       (dependent-producer (make 'single-input-producer
                                 :name "dependent"
                                 :direct-dependencies (list "test-producer")))
       (*producers-collection* (make 'producers-collection)))
  (register-producer* *producers-collection* producer)
  (register-producer* *producers-collection* dependent-producer)
  (prove:is-error (dependencies-tree producer 1 (extra-arguments)) 'circular-dependency))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer :name "producer-1")))
  (register-producer producer)
  (let ((tree (dependencies-tree producer '(1) (extra-arguments))))
    (prove:is-type tree 'list)
    (prove:is (~> tree first producer) producer)
    (prove:is (~> tree first ids) 1)
    (prove:is (~> tree first product) nil)
    (prove:is (~> tree first parent) nil)))


(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer :name "producer-1")))
  (register-producer producer)
  (let ((tree (dependencies-tree producer '(1 2) (extra-arguments))))
    (prove:is-type tree 'list)
    (prove:is (length tree) 2)
    (prove:is (~> tree first producer) producer)
    (prove:is (~> tree second producer) producer)
    (prove:is (mapcar #'ids tree) '(1 2))
    (prove:is (~> tree first parent) nil)
    (prove:is (~> tree second parent) nil)))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :direct-dependencies (list "producer-1"))))
  (register-producer producer)
  (prove:is-error (dependencies-tree producer '(1 2) (extra-arguments))
                  'circular-dependency))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2"
                                  :direct-dependencies (list "producer-1"))))
  (register-producer producer)
  (register-producer producer-2)
  (prove:is-error (dependencies-tree producer '(1 2) (extra-arguments))
                  'circular-dependency))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2")))
  (register-producer producer)
  (register-producer producer-2)
  (let ((tree (dependencies-tree producer '(1 2) (extra-arguments))))
    (prove:is (~> tree first direct-dependencies first producer)
              producer-2)))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :postgres-dao (make 'mock-id-holder :id 1)
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :postgres-dao (make 'mock-id-holder :id 2)
                                  :name "producer-2")))
  (register-producer producer)
  (register-producer producer-2)
  (let* ((tree (dependencies-tree producer '(1) (extra-arguments)))
         (*products-group-state* (make-products-group-state* :mock
                                                             (make 'mock-id-holder :id 0)
                                                             tree)))
    (prove:is-type *products-group-state* 'products-group-state)
    (prove:is (~> *products-group-state* pending-products length) 1)
    (prove:is (~> *products-group-state* pending-products first-elt producer) producer-2)
    (prove:is (~> tree first direct-dependencies first producer) producer-2)
    (prove:is (~> *products-group-state* dependent-map hash-table-count) 2)
    (prove:is (~> *products-group-state* dependencies-map hash-table-count) 2)
    (let ((dependent-map (dependent-map *products-group-state*)))
      (prove:is (gethash (product-key producer '(1) nil) dependent-map) (list))
      (prove:is (gethash (product-key producer-2 '(1) nil) dependent-map) (list (first (flatten tree)))))
    (let ((dependencies-map (dependencies-map *products-group-state*)))
      (prove:is (gethash (product-key producer '(1) nil) dependencies-map) (~> tree flatten first direct-dependencies
                                                                               first-elt list))
      (prove:is (gethash (product-key producer-2 '(1) nil) dependencies-map) (list)))))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :postgres-dao (make 'mock-id-holder :id 1)
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2"
                                  :postgres-dao (make 'mock-id-holder :id 2)))
       (producer-3 (make-instance 'single-input-mock-producer
                                  :name "producer-3"
                                  :postgres-dao (make 'mock-id-holder :id 3)
                                  :direct-dependencies (list "producer-2"))))
  (register-producer producer)
  (register-producer producer-2)
  (register-producer producer-3)
  (let* ((tree-1 (dependencies-tree producer '(1) (extra-arguments)))
         (tree-2 (dependencies-tree producer-3 '(1) (extra-arguments)))
         (*products-group-state* (make-products-group-state*
                                  :mock (make 'mock-id-holder :id 0)
                                  (list tree-1 tree-2))))
    (propagate-failure (~> tree-1 flatten first direct-dependencies first-elt)
                       *products-group-state*)
    (prove:is (~> *products-group-state* pending-products length) 1) ; propagate-failure does not remove node from the pending list, just adds the new ones
    (prove:is (~> *products-group-state* waiting-products length) 0)
    *products-group-state*))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :postgres-dao (make 'mock-id-holder :id 1)
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2"
                                  :postgres-dao (make 'mock-id-holder :id 2)))
       (producer-3 (make-instance 'single-input-mock-producer
                                  :name "producer-3"
                                  :postgres-dao (make 'mock-id-holder :id 3)
                                  :direct-dependencies (list "producer-2"))))
  (register-producer producer)
  (register-producer producer-2)
  (register-producer producer-3)
  (let* ((tree-1 (dependencies-tree producer '(1) (extra-arguments)))
         (tree-2 (dependencies-tree producer-3 '(1) (extra-arguments)))
         (*products-group-state* (make-products-group-state*
                                  :mock (make 'mock-id-holder :id 0)
                                  (list tree-1 tree-2)))
         (pending-products (pending-products *products-group-state*))
         (first-node (first-elt pending-products))
         (completed-products (completed-products *products-group-state*))
         (waiting-products (waiting-products *products-group-state*)))
    (dependency-tree-node-map (lambda (node) (change-class node 'mock-dependency-tree-node) t)
                              (list tree-1 tree-2))
    (prove:is (length waiting-products) 2)
    (prove:ok (not (fullfilled first-node)))
    (try-fullfill* *products-group-state* 0)
    (prove:is (hash-table-count completed-products) 1)
    (prove:ok (fullfilled first-node))
    (prove:is (length waiting-products) 0)
    (prove:is (length pending-products) 2)))


(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :postgres-dao (make 'mock-id-holder :id 1)
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2"
                                  :postgres-dao (make 'mock-id-holder :id 2)))
       (producer-3 (make-instance 'single-input-mock-producer
                                  :name "producer-3"
                                  :postgres-dao (make 'mock-id-holder :id 3)
                                  :direct-dependencies (list "producer-2"))))
  (register-producer producer)
  (register-producer producer-2)
  (register-producer producer-3)
  (let* ((tree-1 (dependencies-tree producer '(1) (extra-arguments)))
         (tree-2 (dependencies-tree producer-3 '(1) (extra-arguments)))
         (*products-group-state* (make-products-group-state*
                                  :mock (make 'mock-id-holder :id 0)
                                  (list tree-1 tree-2)))
         (pending-products (pending-products *products-group-state*))
         (completed-products (completed-products *products-group-state*))
         (waiting-products (waiting-products *products-group-state*)))
    (dependency-tree-node-map (lambda (node) (change-class node 'mock-dependency-tree-node) t)
                              (list tree-1 tree-2))
    (fullfill-products-group-state* :mock *products-group-state*)
    (prove:is (length pending-products) 0)
    (prove:is (length waiting-products) 0)
    (prove:is (hash-table-count completed-products) 3)))


(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :postgres-dao (make 'mock-id-holder :id 1)
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2"
                                  :postgres-dao (make 'mock-id-holder :id 2)))
       (producer-3 (make-instance 'single-input-mock-producer
                                  :name "producer-3"
                                  :postgres-dao (make 'mock-id-holder :id 3)
                                  :direct-dependencies (list "producer-2"))))
  (register-producer producer)
  (register-producer producer-2)
  (register-producer producer-3)
  (let* ((tree-1 (dependencies-tree producer '(1) (extra-arguments)))
         (tree-2 (dependencies-tree producer-3 '(1) (extra-arguments)))
         (*products-group-state* (make-products-group-state*
                                  :mock (make 'mock-id-holder :id 0)
                                  (list tree-1 tree-2))))
    (dependency-tree-node-map (lambda (node) (change-class node 'mock-dependency-tree-node :future-error (make-condition 'error)) t)
                              (list tree-1 tree-2))
    (prove:is-condition (try-fullfill* *products-group-state* 0)
                        'single-demo-failed)))

(defun restart-invoking (restart)
  (lambda (x) (declare (ignore x))
    (invoke-restart restart)))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :postgres-dao (make 'mock-id-holder :id 1)
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2"
                                  :postgres-dao (make 'mock-id-holder :id 2)))
       (producer-3 (make-instance 'single-input-mock-producer
                                  :name "producer-3"
                                  :postgres-dao (make 'mock-id-holder :id 3)
                                  :direct-dependencies (list "producer-2"))))
  (register-producer producer)
  (register-producer producer-2)
  (register-producer producer-3)
  (let* ((tree-1 (dependencies-tree producer '(1) (extra-arguments)))
         (tree-2 (dependencies-tree producer-3 '(1) (extra-arguments)))
         (*products-group-state* (make-products-group-state*
                                  :mock (make 'mock-id-holder :id 0)
                                  (list tree-1 tree-2)))
         (waiting-products (waiting-products *products-group-state*))
         (pending-products (pending-products *products-group-state*))
         (completed-products (completed-products *products-group-state*)))
    (dependency-tree-node-map (lambda (node) (change-class node 'mock-dependency-tree-node :future-error (make-condition 'error)) t)
                              (list tree-1 tree-2))
    (handler-bind ((single-demo-failed (restart-invoking 'skip-demo-id)))
      (try-fullfill* *products-group-state* 0))
    (prove:is (length waiting-products) 0)
    (prove:is (length pending-products) 2)
    (prove:is (hash-table-count completed-products) 0)))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :postgres-dao (make 'mock-id-holder :id 1)
                                :direct-dependencies (list "producer-2")))
       (producer-2 (make-instance 'single-input-mock-producer
                                  :name "producer-2"
                                  :postgres-dao (make 'mock-id-holder :id 2)))
       (producer-3 (make-instance 'single-input-mock-producer
                                  :name "producer-3"
                                  :postgres-dao (make 'mock-id-holder :id 3)
                                  :direct-dependencies (list "producer-2"))))
  (register-producer producer)
  (register-producer producer-2)
  (register-producer producer-3)
  (let* ((tree-1 (dependencies-tree producer '(1) (extra-arguments)))
         (tree-2 (dependencies-tree producer-3 '(1) (extra-arguments)))
         (products-group-state (make-products-group-state*
                                :mock (make 'mock-id-holder :id 0)
                                (list tree-1 tree-2))))
    (prove:is (~> products-group-state pending-products length) 1)
    (prove:is (~> products-group-state waiting-products length) 2)
    (propagate-success (~> tree-1 flatten first direct-dependencies first-elt)
                       products-group-state)
    (~> products-group-state pending-products print)
    (prove:is (~> products-group-state pending-products length) 3) ; propagate-success does not remove node from the pending list, just adds the new ones
    (prove:is (~> products-group-state waiting-products length) 0)))

(let* ((*config* (make-instance 'fundamental-config
                                :dao-controller :mock))
       (*producers-collection* (make 'producers-collection))
       (producer (make-instance 'single-input-mock-producer
                                :name "producer-1"
                                :direct-dependencies (list "producer-2"))))
  (prove:is-error (dependencies-tree producer '(1 2) (extra-arguments))
                  'producer-name-not-found))

(prove:finalize)
