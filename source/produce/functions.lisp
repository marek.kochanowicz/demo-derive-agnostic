(cl:in-package #:demo-derive.produce)


(defun register-producer (producer)
  (register-producer* *producers-collection* producer))


(defun find-producer-by-id (id)
  (find-producer-by-id* *producers-collection* id))


(defun find-producer (name)
  (find-producer* *producers-collection* name))


(defun dependencies-tree (producer ids extra-arguments)
  (dependencies-tree* producer ids extra-arguments
                      (cl-ds.dicts.hamt:make-transactional-hamt-dictionary
                       #'sxhash #'equal)))


(defun send-producers-collection-to-database ()
  (with-postgres-connection (*config*)
    (send-producers-collection-to-database* *producers-collection*)))


(defun fetch-producers-collection-from-database ()
  (with-postgres-connection (*config*)
    (fetch-producers-collection-from-database* *producers-collection*)))


(defun parse-python-manifest-file (path-to-manifest)
  (bind ((directory (pathname-directory path-to-manifest))
         (yason:*parse-json-arrays-as-vectors* t)
         (parsed (with-open-file (stream path-to-manifest)
                   (yason:parse stream)))
         (dependencies (map 'list
                             (lambda (object)
                               (if (hash-table-p object)
                                   (cons (gethash "name" object)
                                         (parsed-json->producer-parameters (gethash "parameters" object)))
                                   object))
                             (gethash "dependencies" parsed '())))
         (mode (gethash "mode" parsed "single"))
         (table-name (gethash "table-name" parsed #()))
         (schema (gethash "schema" parsed))
         (name (gethash "name" parsed))
         (create-script (gethash "create" parsed))
         (delete-script (gethash "delete" parsed)))
    (assert (member mode '("single" "multiple") :test 'equal) (mode) mode)
    (assert (stringp name) (name) name)
    (make (econd ((null table-name)
                   (if (string= "single" mode)
                       'single-input-python-producer
                       'multiple-input-python-producer))
                  ((stringp table-name)
                   (if (string= "single" mode)
                       'single-table-single-input-python-producer
                       'single-table-multiple-input-python-producer))
                  ((vectorp table-name)
                   (if (string= "single" mode)
                       'single-input-python-producer
                       'multiple-input-python-producer)))
           :table-name table-name
           :create-path (if (null create-script)
                            nil
                            (make-pathname :directory directory
                                           :name create-script
                                           :type "py"))
           :delete-path (if (null delete-script)
                            nil
                            (make-pathname :directory directory
                                           :name delete-script
                                           :type "py"))
           :path-to-schema (if schema
                               (make-pathname :directory directory
                                              :name schema
                                              :type "sql")
                               nil)
          :direct-dependencies dependencies
          :name name)))


(defun find-and-register-python-producers ()
  (~> (cl-ds.fs:find `((:directory :path ,(python-producers-path *config*))
                       (:all-directories)
                       (:regex-file :path "manifest\\.json")))
      (cl-ds.alg:on-each #'parse-python-manifest-file)
      (cl-ds.alg:on-each #'register-producer)
      cl-ds.alg:to-list))


(defun invoke-with-sequence (extra-arguments producers ids
                             &key (information :null) lifetime (automatic-deletion nil))
  (check-type ids sequence)
  (check-type producers (or string sequence))
  (ensure extra-arguments
    (cl-ds.dicts.hamt:make-transactional-hamt-dictionary #'sxhash #'equal))
  (map nil (lambda (id) (check-type id non-negative-fixnum)) ids)
  (with-postgres-connection (*config*)
    (let ((products-group (make-postprocessing-products-group))
          (dependencies-trees nil)
          (products-group-state nil)
          (*group-log-stream* (make-string-output-stream)))
      (setf (information products-group) information)
      (unless (null lifetime)
        (setf (expiration-date products-group)
              (local-time:timestamp+ (local-time:now)
                                     lifetime
                                     :day)))
      (unwind-protect
           ;; this will also signal error if circular dependency is detected
           (progn
             (handler-bind
                 ((error (lambda (e) (format *group-log-stream* "~a~%" e))))
               (setf dependencies-trees (mapcar (compose (rcurry #'dependencies-tree ids extra-arguments)
                                                         #'find-producer)
                                                producers)
                     products-group-state (make-products-group-state*
                                           (dao-controller *config*)
                                           products-group
                                           dependencies-trees
                                           :automatic-deletion automatic-deletion)))
             (handler-bind
                 ((error (lambda (e) (when (boundp '*log-stream*)
                                  (format *log-stream* "~a~%" e)))))
               (fullfill-products-group-state* (dao-controller *config*)
                                               products-group-state))
             products-group)
        (when (eq :null (success products-group))
          (setf (success products-group) nil))
        (setf (logs products-group) (get-output-stream-string *group-log-stream*))
        (update-dao products-group)))))


(defun invoke (extra-arguments producers id information &rest more-ids)
  (invoke-with-sequence extra-arguments
                        producers
                        (cons id more-ids)
                        information))


(defun parse-config (path-to-config)
  (bind ((dict (with-open-file (stream path-to-config)
                 (yason:parse stream)))
         ((:flet at (name &optional (mandatory-p t)))
          (bind (((:values result found) (gethash name dict)))
            (when (and mandatory-p (not found))
              (error "Missing mandatory field in the config!"))
            (when (stringp result)
              (setf result (coerce result 'simple-string)))
            result)))
    (make 'fundamental-config
          :postgres-connection-list (list (at "database_name")
                                          (at "database_user")
                                          (at "database_password")
                                          (at "database_host")
                                          :use-ssl :try)
          :python-producers-path (at "python_producers_path" nil))))


(defun extra-arguments (&rest arguments)
  (iterate
    (with result =
          (cl-ds.dicts.hamt:make-transactional-hamt-dictionary #'sxhash #'equal))
    (for (key parameters) in (batches arguments 2))
    (setf (cl-ds:at result key) (sort-producer-parameters parameters))
    (finally (return result))))


(defun make-direct-dependencies-tree-nodes (producer ids extra-arguments
                                            parent dependent-producers)
  (flatten (bind (((:values dependencies arguments) (direct-dependencies producer))
                  (dependent-producers (cl-ds:replica dependent-producers))
                  (this-arguments (cl-ds:at extra-arguments (name producer))))
             (when (shiftf (cl-ds:at dependent-producers
                                     (product-key producer ids this-arguments))
                           t)
               (error 'circular-dependency))
             (iterate
               (for dependency in dependencies)
               (for argument in arguments)
               (for augmented-arguments
                    = (if (null argument)
                          extra-arguments
                          (augment-extra-arguments (first argument)
                                                   (name dependency)
                                                   extra-arguments)))
               (collecting (dependencies-tree* dependency ids
                                               augmented-arguments
                                               dependent-producers
                                               parent))))))


(defun existing-products (producer ids producer-parameters)
  (existing-products* (dao-controller *config*)
                      producer ids
                      producer-parameters))


(defun make-postprocessing-products-group ()
  (make-postprocessing-products-group* (dao-controller *config*)))


(defun make-postprocessing-product (&rest initargs)
  (apply #'make-postprocessing-product*
         (dao-controller *config*)
         initargs))


(defun make-postprocessing-product-dependency (&rest initargs)
  (apply #'make-postprocessing-product-dependency*
         (dao-controller *config*)
         initargs))


(defun update-dao (object)
  (update-dao* (dao-controller *config*) object))


(defun delete-product (producer product)
  (delete-product* (dao-controller *config*)
                   producer
                   product))


(defun make-products-group-state (products-group dependencies-tree
                                  &rest rest
                                  &key (automatic-deletion nil))
  (declare (ignore automatic-deletion))
  (apply #'make-products-group-state*
         (dao-controller *config*)
         products-group
         dependencies-tree
         rest))


(defun dependency-tree-node-map (function node)
  (ensure-functionf function)
  (labels ((impl (node)
             (when (funcall function node)
               (map nil
                    #'impl
                    (flatten (mapcar #'direct-dependencies (flatten node)))))))
    (map nil #'impl (flatten node))))


(defun sync-dao (object)
  (sync-dao* (dao-controller *config*)
             object))


(defun lock-product (object)
  (lock-product* (dao-controller *config*) object))


(defun call-preamble (arguments ids)
  (bind ((k.preamble (assoc :preamble arguments :test 'eq)))
    (unless (null k.preamble)
      (funcall (cdr k.preamble) (ids-list ids)))))


(defun call-cleanup (arguments ids)
  (bind ((k.cleanup (assoc :cleanup arguments :test 'eq)))
    (unless (null k.cleanup)
      (funcall (cdr k.cleanup) (ids-list ids)))))
