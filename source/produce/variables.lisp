(cl:in-package #:demo-derive.produce)


(defparameter *config* nil)

(defvar *producers-collection* (make 'producers-collection))
(defvar *memoization* t)
(defvar *top-level-memo-table* nil)
(defvar *products-group-state*)
(defvar *log-stream* (make-broadcast-stream))
(defvar *group-log-stream*)
(defvar *status-stream* (make-broadcast-stream))
(defvar *python* nil)
(defvar *current-product* nil)
