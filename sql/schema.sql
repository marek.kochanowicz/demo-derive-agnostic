create schema if not exists postprocessing;
grant usage on schema postprocessing to public;

create table if not exists postprocessing.producers (
  id serial not null,
  name varchar not null,
  CONSTRAINT producers_pkey PRIMARY KEY (id)
);

create table if not exists postprocessing.products_groups (
  id serial not null,
  success bool null,
  information jsonb null,
  logs varchar null,
  expiration_date timestamptz,
  CONSTRAINT products_group_pkey PRIMARY KEY (id)
);

create table if not exists postprocessing.products (
  id serial not null,
  producer_id int4 not null,
  products_group_id int not null,
  demo_id int4[],
  production_date timestamptz,
  success bool,
  producer_parameters jsonb,
  logs varchar,
  processing_time int4,
  canceled bool,
  constraint products_primary_key primary key (id),
  CONSTRAINT products_products_groups foreign key(products_group_id) references postprocessing.products_groups on delete cascade,
  CONSTRAINT products_products_producers foreign key(producer_id) references postprocessing.producers(id)
);

create table if not exists postprocessing.products_group_product (
  products_group_id int not null,
  product_id int not null,
  CONSTRAINT products_products_groups foreign key(products_group_id) references postprocessing.products_groups on delete cascade,
  CONSTRAINT products_products_product foreign key(product_id) references postprocessing.products on delete cascade
);

create table if not exists postprocessing.product_dependencies (
  id serial not null,
  dependent int4 not null,
  depends_on int4 not null,
  constraint product_dependencies_pkey primary key (id)
);

grant select on table postprocessing.products_groups to public;
grant select on table postprocessing.producers to public;
grant select on table postprocessing.products to public;
grant select on table postprocessing.product_dependencies to public;
grant select on table postprocessing.products_group_product to public;
