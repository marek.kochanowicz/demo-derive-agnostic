(asdf:defsystem #:demo-derive-agnostic
  :name "demo-derive-agnostic"
  :version "0.0.0"
  :author "Marek Kochanowicz"
  :depends-on ( :iterate
                :serapeum
                :metabang-bind
                :cl-data-structures
                :postmodern-localtime
                :trivial-garbage
                :vellum-postmodern
                :alexandria
                :yason
                :cl+ssl
                :trivial-signal
                :cl-progress-bar
                :postmodern)
  :serial T
  :pathname "source"
  :components ((:file "aux-package")
               (:module "common"
                :components ((:file "package")
                             (:file "memo")))
               (:module "produce"
                :components ((:file "package")
                             (:file "generics")
                             (:file "macros")
                             (:file "types")
                             (:file "variables")
                             (:file "utils")
                             (:file "functions")
                             (:file "methods")))))
